"use strict";

var Promise = require("bluebird");
var _ = require("lodash");
var startOfWeek = require("date-fns/start_of_week");
var isToday = require("date-fns/is_today");
var subWeeks = require("date-fns/sub_weeks");
var addWeeks = require("date-fns/add_weeks");
var getHours = require("date-fns/get_hours");
var setHours = require("date-fns/set_hours");
var format = require("date-fns/format");
var collectResults = require("./collect-results");
var isAfter = require("date-fns/is_after");

function projectFromWebUrl(webUrl) {
  return webUrl.replace(/^https:\/\/gitlab.com\//, "").replace(/\/(issues|merge_requests)\/.*$/, "");
}

function attributeIssueClosure(issue) {
  if (issue.assignees.length) {
    return issue.assignees.map(user => `@${user.username}`).join(", ");
  }

  if (issue.closed_by) {
    return `@${issue.closed_by.username}`;
  }

  return "";
}

const ISSUE_WEIGHT_EMOJI = "zero|one|two|three|four|five|six|seven|eight|nine|keycap_ten".split("|");

function weightAsEmoji(issueWeight) {
  if (!issueWeight) return ":hash:";

  let emoji = ISSUE_WEIGHT_EMOJI[issueWeight];
  if (emoji) return `:${emoji}:`;

  return ":arrow_up:";
}

function formatIssue(issue, filterLabels, includeUser) {
  let project = projectFromWebUrl(issue.web_url);

  issue.labels.sort();

  let labels = issue.labels.filter(label => !filterLabels[label]).map(label => `${project}~"${label}"`);

  let milestone = issue.milestone && issue.milestone.iid && !issue.milestone.group_id ? ` ${project}%${issue.milestone.iid}` : "";

  let assignees = "";
  if (includeUser) {
    let assigneeList = attributeIssueClosure(issue);
    assignees = assigneeList ? ` by ${assigneeList}` : "";
  }

  let weightEmoji = weightAsEmoji(issue.weight);

  return ` * ${weightEmoji} ${project}#${issue.iid} ${issue.title} ${labels.join(" ")}${milestone}${assignees}`;
}

function calculateOpenMilestoneWeight() {
  return collectResults("https://gitlab.com/api/v4/projects/gitlab-com%2fmigration/milestones?state=active").then(milestones => {
    return Promise.map(milestones, milestone => {
      return collectResults(
        `https://gitlab.com/api/v4/projects/gitlab-com%2fmigration/issues?milestone=${encodeURIComponent(milestone.title)}&state=opened`
      ).then(issues => {
        let issueWeight = issues.reduce((memo, issue) => memo + issue.weight || 0, 0);
        return [milestone, issueWeight];
      });
    });
  });
}

function formatIssues(header, issues, filterLabels, includeUser = true) {
  if (!issues.length) return "";

  let markdown = issues.map(issue => formatIssue(issue, filterLabels, includeUser)).join("\n");

  return `
## ${header}

${markdown}
`;
}

function reportIssuesClosed(label, since) {
  let sinceIso = format(since, "YYYY-MM-DD");

  return collectResults(`https://gitlab.com/api/v4/issues?state=closed&scope=all&labels=${label}&order_by=updated_at&sort=desc&updated_after=${sinceIso}`).then(
    issues => issues.filter(issue => isAfter(issue.closed_at, since))
  );
}

function reportIssuesOpened(label, since) {
  let sinceIso = format(since, "YYYY-MM-DD");

  return collectResults(`https://gitlab.com/api/v4/issues?state=all&scope=all&labels=${label}&order_by=created_at&created_after=${sinceIso}`);
}

function reportMigrationIssuesClosed(since) {
  let sinceIso = format(since, "YYYY-MM-DD");

  return collectResults(
    `https://gitlab.com/api/v4/projects/gitlab-com%2fmigration/issues?state=closed&scope=all&order_by=updated_at&sort=desc&updated_after=${sinceIso}`
  ).then(issues => issues.filter(issue => isAfter(issue.closed_at, since)));
}

function reportMigrationIssuesOpened(since) {
  let sinceIso = format(since, "YYYY-MM-DD");

  return collectResults(`https://gitlab.com/api/v4/projects/gitlab-com%2fmigration/issues?state=all&scope=all&order_by=created_at&created_after=${sinceIso}`);
}

function startTime() {
  var now = new Date();

  var friday = startOfWeek(now, { weekStartsOn: 5 });

  if (isToday(friday)) {
    if (getHours(now) < 16) {
      friday = subWeeks(friday, 1);
    }
  }

  // Switchover time is always 16h00UTC on a Friday
  return setHours(friday, 16);
}

function deduplicateIssues(listsOfIssues) {
  let hash = {};

  listsOfIssues.forEach(issues => {
    issues.forEach(issue => (hash[issue.web_url] = issue));
  });

  return _.sortBy(Object.values(hash), issue => issue.web_url);
}

function filterIssues(issues, sort = "closed_at") {
  issues = issues.filter(issue => !issue.labels.includes("Status Report") && !issue.confidential);
  return _.sortBy(issues, sort);
}

function generateMilestoneSummary(milestoneWeights, openMilestoneWeights, daysSinceStart, openMilestones) {
  let milestoneTitles = Object.keys(openMilestones);
  milestoneTitles.sort();

  return milestoneTitles
    .map(title => {
      let closedWeight = milestoneWeights[title] || 0;
      let openedWeight = openMilestoneWeights[title] || 0;
      let rate = closedWeight / daysSinceStart;
      let remaining = openMilestones[title];
      let daysRemaining = rate > 0 ? `${(remaining / rate).toFixed(1)} days remaining at current pace` : "No progress this cycle";
      let rateString = rate.toFixed(1);

      return `%"${title}": ${closedWeight} issue points closed, ${rateString} per day. ${openedWeight} issue points created. ${remaining} points remaining. ${daysRemaining}`;
    })
    .join("\n\n");
}

function calculateWorkDaysSince(since) {
  // Exclude the weekend
  let days = (Date.now() - since) / 86400000;
  if (days <= 1) return days;
  if (days > 1 && days < 2) return 1;
  return days - 2;
}

async function findClosedIssuesAndScoreMilestones(since) {
  let milestoneWeights = {};

  let issues = await Promise.all([
    reportIssuesClosed("GCP%20Migration", since),
    reportIssuesClosed("Object%20Storage", since),
    reportMigrationIssuesClosed(since).then(issues => {
      issues.forEach(issue => {
        if (!issue.weight || !issue.milestone) return;

        milestoneWeights[issue.milestone.title] = (milestoneWeights[issue.milestone.title] || 0) + issue.weight;
      });

      return issues;
    })
  ]);

  return [issues, milestoneWeights];
}

async function findIssuesOpenedAndScoreMilestones(since) {
  let milestoneWeights = {};

  let issues = await Promise.all([
    reportIssuesOpened("GCP%20Migration", since),
    reportIssuesOpened("Object%20Storage", since),
    reportMigrationIssuesOpened(since).then(issues => {
      issues.forEach(issue => {
        if (!issue.weight || !issue.milestone) return;

        milestoneWeights[issue.milestone.title] = (milestoneWeights[issue.milestone.title] || 0) + issue.weight;
      });

      return issues;
    })
  ]);

  return [issues, milestoneWeights];
}

async function getStatusReport() {
  let since = startTime();
  let until = addWeeks(since, 1);

  let milestones = await calculateOpenMilestoneWeight();

  let openMilestones = milestones.reduce((memo, item) => {
    memo[item[0].title] = item[1];
    return memo;
  }, {});

  let [listsOfIssues, milestoneWeights] = await findClosedIssuesAndScoreMilestones(since);
  let [listsOfOpenedIssues, openMilestoneWeights] = await findIssuesOpenedAndScoreMilestones(since);

  let issues = deduplicateIssues(listsOfIssues);
  issues = filterIssues(issues);

  let openedIssues = deduplicateIssues(listsOfOpenedIssues);
  openedIssues = filterIssues(openedIssues, "created_at");

  if (!issues.length && !openedIssues.legnth) return;

  let closedIssuesMarkdown = formatIssues("Issues Closed in this iteration", issues, {
    "GCP Migration": true
  });

  let openedIssuesMarkdown = formatIssues(
    "New migration issues opened in this iteration",
    openedIssues,
    {
      "GCP Migration": true
    },
    false
  );

  let totalIssueWeightClosed = issues.reduce((memo, issue) => memo + issue.weight, 0);
  let totalIssueWeightOpened = openedIssues.reduce((memo, issue) => memo + issue.weight, 0);
  let daysSinceStart = calculateWorkDaysSince(since);
  let currentRunRate = totalIssueWeightClosed / daysSinceStart;
  let weightPerMilestoneSummary = generateMilestoneSummary(milestoneWeights, openMilestoneWeights, daysSinceStart, openMilestones);

  return {
    since: since,
    until: until,
    title: `GCP Migration Status Report: ${format(since, "YYYY-MM-DD")} - ${format(until, "YYYY-MM-DD")}`,
    body: `
# Migration

${closedIssuesMarkdown}

${openedIssuesMarkdown}

## Summary

Total issue points closed this cycle: **${totalIssueWeightClosed}**

Total issue points opened this cycle: **${totalIssueWeightOpened}**

Current run rate: **${currentRunRate.toFixed(1)} points per day**

${weightPerMilestoneSummary ? "### Per Milestone " : ""}

${weightPerMilestoneSummary}

-------------------------

This status report was [autogenerated](https://gitlab.com/andrewn/gcp-status/).
`
  };
}

module.exports = getStatusReport;
